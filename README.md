# Contao E-Mail Protection Bundle

## About
Mit diesem Bundle können per Insert Tag E-Mail Adressen für Mail Harvester unsichtbar gemacht werden.
Falls JS deaktiviert ist werden die E-Mail Adressen per Captcha geschützt.

### Features
- [x] noJS Fallback mittels Captcha Eingabe
- [x] E-Mail Adresse mittels UUID verschlüsselt
- [x] Konfiguration pro Seitenbaum (RootPage)
- [x] Frontend Modul für individuelle Ausgabe der Captcha Seite  
- [x] Ausgabe im Quellcode mit unsichtbaren Zeichen angereichert
- [x] Click Tracking und Listing der geschützten Adressen im Backend

## Installation
Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repo (not on packagist.org) to your composer.json:
```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-mailprotection-bundle.git"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/contao-mailprotection-bundle": "^0.0",
```

## Usage (German)
1. Memo Mail Protection Bundle installieren
2. Seite für Redirect (JS Fallback) erstellen
3. Setup Memo Mail Protection Bundle (Hinterlegen der Einstellungen pro RootPage)
3. Frontend Modul erstellen/konfigurieren (Memo Captcha)
4. Auf der Inhaltsseite (JS Fallback) das Frontend Modul einbinden und Darstellung anpassen.
5. zu schützende E-Mail Adressen per Inserttag markieren.

### Folgende Inserttags stehen zur Verfügung

Inserttag | Beschreibung
--------- | ------------
``` {{memoEmail::*}}``` | Dieses Tag wird mit einem kodierten Link zu einer E-Mail-Adresse ersetzt.
``` {{memoEmail_url::*}}``` | Dieses Tag wird nur durch die kodierte E-Mail-Adresse ersetzt.
```{{memoEmail_url_open}}```| Dieser Tag wird mit einem öffnenden Anker-Tag ersetzt.

## Contribution
Bug reports and pull requests are welcome


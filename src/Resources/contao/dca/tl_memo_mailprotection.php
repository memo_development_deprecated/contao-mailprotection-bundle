<?php

/**
 * Contao Open Source CMS
 * @package   Memo MailProtectionBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Table tl_memo_mailprotection
 */
$GLOBALS['TL_DCA']['tl_memo_mailprotection'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'switchToEdit'                => false,
        'enableVersioning'            => false,
        'notCreatable'                => true,
        'notCopyable'				  => true,
        'notDeletable'				  => true,
        'notEditable'				  => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'sorting' => 'index'
            )
        )
    ),

    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('email','hash','clicks'),
            'panelLayout'             => 'filter;sort,search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('email','hash','clicks'),
            'format'                  => '%s <span style="color:#999;padding-left:3px">[%s] (%s)</span>',
            'showColumns'             => true

        ),
        'global_operations' => array
        (

        ),
        'operations' => array
        (

            'show' => array
            (
                'href'                => 'act=show',
                'icon'                => 'show.svg'
            ),
        )
    ),

    'palettes' => array
    (
        '__selector__'                => array('protected'),
        'default'                     => ''
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'label'                   => array('ID'),
            'search'                  => false,
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default 0"
        ),
        'ptable' => array
        (
            'sql'                     => "varchar(64) NOT NULL default ''"
        ),
        'email' => array
        (
            'label'                   => $GLOBALS['TL_LANG']['tl_memo_mailprotection']['email'],
            'sql'                     => "varchar(255) default NULL",
            'filter'                  => true,
            'search'                  => true
        ),
        'hash' => array
        (
            'label'                   => $GLOBALS['TL_LANG']['tl_memo_mailprotection']['hash'],
            'sql'                     => "varchar(255) default NULL ",
            'filter'                  => true,
            'search'                  => true,
        ),

        'sorting' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default 0"
        ),
        'clicks' => array
        (
            'label'                   => $GLOBALS['TL_LANG']['tl_memo_mailprotection']['clicks'],
            'sql'                     => "int(10) unsigned NOT NULL default 0"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default 0"
        )
    )
);

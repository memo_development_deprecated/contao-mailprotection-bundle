<?php

/**
 * Contao Open Source CMS
 * @package   Memo MailProtectionCaptcha
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


$GLOBALS['TL_DCA']['tl_module']['palettes']['memo_mailprotection_captcha'] = '{title_legend}, name, headline,type;{captcha_legend},length,fontSize,mpbBackground;{template_legend:hide},archive_template,customTpl;{expert_legend:hide},cssID;';



$GLOBALS['TL_DCA']['tl_module']['fields']['mpbBackground'] = [
        'label'                   => &$GLOBALS['TL_LANG']['tl_module']['mpbBackground'],
        'exclude'                 => true,
        'inputType'               => 'fileTree',
        'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>false, 'tl_class'=>'w50 clr'),
        'sql'                     => "binary(16) NULL"
    ];

$GLOBALS['TL_DCA']['tl_module']['fields']['length'] = [
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['text'],
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'select',
    'options'                 => [1,2,3,4,5,6,7,8,9,10],
    'eval'                    => array('mandatory'=>false,'tl_class'=>'w50'),
    'sql'                     => "int(11) unsigned NOT NULL default 6"
];

$GLOBALS['TL_DCA']['tl_module']['fields']['fontSize'] = [
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['fontSize'],
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false,'tl_class'=>'w50','rgxp'=>'digit','tl_class'=>'w50'),
    'sql'                     => "int(11) unsigned NOT NULL default 50"
];

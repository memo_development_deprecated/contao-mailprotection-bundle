<?php
/**
 * Contao Open Source CMS
 *
 * @package   Memo MailProtectionBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright 2021,Media Motion AG
 */


use Contao\CoreBundle\DataContainer\PaletteManipulator;

$intVersion = (float) \Controller::replaceInsertTags('{{version}}');

if($intVersion >= 4.9){
	PaletteManipulator::create()
    ->addLegend('memoMailProtection', 'dns_legend', PaletteManipulator::POSITION_BEFORE)
    ->addField('mbpJumpTo', 'memoMailProtection', PaletteManipulator::POSITION_APPEND)
    ->addField('mpbOptions', 'memoMailProtection', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('root','tl_page')
    ->applyToPalette('rootfallback','tl_page');
} else {
    PaletteManipulator::create()
    ->addLegend('memoMailProtection', 'dns_legend', PaletteManipulator::POSITION_BEFORE)
    ->addField('mbpJumpTo', 'memoMailProtection', PaletteManipulator::POSITION_APPEND)
    ->addField('mpbOptions', 'memoMailProtection', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('root','tl_page');
}

$GLOBALS['TL_DCA']['tl_page']['fields']['mpbOptions'] = [
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array('simple', 'extend'),
    'reference'               => &$GLOBALS['TL_LANG']['tl_page'],
    'eval'                    => array('includeBlankOption'=>false, 'feGroup'=>'personal', 'tl_class'=>'w50'),
    'sql'                     => "varchar(32) NOT NULL default ''"
];


$GLOBALS['TL_DCA']['tl_page']['fields']['mbpJumpTo'] = [
    'exclude'                 => true,
    'inputType'               => 'pageTree',
    'foreignKey'              => 'tl_page.title',
    'eval'                    => array('fieldType'=>'radio','tl_class'=>'w50'), // do not set mandatory (see #5453)
    'sql'                     => "int(10) unsigned NOT NULL default 0",
    'relation'                => array('type'=>'hasOne', 'load'=>'lazy')
];

<?php

/**
 * Contao Open Source CMS
 * @package   MailProtectionBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @author    Christian Nussbaumer
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */

$GLOBALS['BE_MOD']['memo_mailprotection']['tl_memo_mailprotection'] = array('tables' => array('tl_memo_mailprotection'));

/**
 * Add front end modules
 */
$GLOBALS['FE_MOD']['memo_mailprotection'] = array
(
    'memo_mailprotection_captcha'    => 'Memo\MailProtectionBundle\Module\MailProtectionCaptcha',
);
/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_memo_mailprotection']                    = 'Memo\MailProtectionBundle\Model\MailProtectionModel';

/**
 * HOOKS (Legacyway for Contao < 4.9)
 */

$intVersion = (float) \Controller::replaceInsertTags('{{version}}');
if($intVersion < 4.9){
	$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('memo.mailprotection.services', 'replaceInsertTags');
}

/**
 * Backend CSS
 */
if(TL_MODE == 'BE')
{
	$GLOBALS['TL_CSS'][] = '/bundles/mailprotection/css/backend.css?v=2021-09-08';
}

<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   mailProtectionBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['tl_memo_mailprotection'] = ['Geschützte Adressen',''];
$GLOBALS['TL_LANG']['MOD']['memo_mailprotection'] = ['Memo Mail Protection',''];

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['memo_mailprotection'] = ['Memo E-Mail Protection',''];
$GLOBALS['TL_LANG']['FMD']['memo_mailprotection_captcha'] = ['Memo Captcha','E-Mail Protection ohne Javascript'];

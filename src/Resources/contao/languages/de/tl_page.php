<?php





$GLOBALS['TL_LANG']['tl_page']['memoMailProtection']  = 'Memo E-Mail Protection';
$GLOBALS['TL_LANG']['tl_page']['mbpJumpTo']  = ['Captcha Redirect Ziel','Seite mit Modul für Captcha Check (noJS Fallback)'];
$GLOBALS['TL_LANG']['tl_page']['mpbOptions']  = ['Sicherheitsstufe'];
$GLOBALS['TL_LANG']['tl_page']['simple']  = ['Standard Contao Sanitizer'];
$GLOBALS['TL_LANG']['tl_page']['extend']  = ['Erweiterte Sicherheit'];
$GLOBALS['TL_LANG']['tl_page']['hardcore']  = ['Ultra Hardcore'];

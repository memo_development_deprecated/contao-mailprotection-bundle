<?php


$GLOBALS['TL_LANG']['tl_module']['captcha_legend'] = 'Captcha Einstellungen';
$GLOBALS['TL_LANG']['tl_module']['mpbBackground'] = ['Captcha Hintergrundbild','Hier können Sie ein Hintergrundbild fürs Captcha definieren'];
$GLOBALS['TL_LANG']['tl_module']['length'] = ['Anzahl Zeichen','Anzahl Zeichen für das Captcha'];
$GLOBALS['TL_LANG']['tl_module']['fontSize'] = ['Schriftgrösse','Die Schriftgrösse des Captchas'];

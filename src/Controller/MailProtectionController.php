<?php

namespace Memo\MailProtectionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Memo\MailProtectionBundle\Model\MailProtectionModel;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Session\Session;


class MailProtectionController extends AbstractController
{

    /**
     * @param null $hash
     * @return RedirectResponse
     */
    public function getMailByHash($hash=null): RedirectResponse
    {
        //No Hash parameter
        if(is_null($hash))
        {
            return new RedirectResponse();
        }
        //Initialize Contao
        $this->get('contao.framework')->initialize();

        $oData = MailProtectionModel::findOneBy('hash',trim($hash));
        if(is_null($oData))
        {
            //No E-Mail found
            return new RedirectResponse();
        }

        $oData->clicks = $oData->clicks+1;
        $oData->save();

        //Return mailto Action
        return new RedirectResponse('mailto:'.$oData->email);
    }


    public function validateCaptcha(Request $request,$prmId=NULL): JsonResponse
    {
        if($request->getMethod() != 'POST')
        {
            return new JsonResponse();
        }

        return new JsonResponse(['success']);
    }

    public function getCaptchaImage(): BinaryFileResponse
    {
        //Initialize Contao
        $this->get('contao.framework')->initialize();
        $tmpName = \System::getContainer()->get('memo.mailprotection.services')->_createCaptchaImage();
        return new BinaryFileResponse($tmpName);
    }


}

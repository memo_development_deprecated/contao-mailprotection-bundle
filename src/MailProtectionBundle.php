<?php
/**
 * @package   MailProtectionBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @author    Christian Nussbaumer
 * @copyright 2021 Media Motion AG
 */

namespace Memo\MailProtectionBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MailProtectionBundle extends Bundle
{
}

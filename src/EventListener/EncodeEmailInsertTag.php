<?php


namespace Memo\MailProtectionBundle\EventListener;
use Contao\CoreBundle\ServiceAnnotation\Hook;
use Memo\MailProtectionBundle\Services\MailProtectionService;

/**
 * @Hook("replaceInsertTags")
 */
class EncodeEmailInsertTag
{
    public $aTags = ['memoEmail','memoEmail_url'];

    public function __invoke(string $tag)
    {
        return MailProtectionService::replaceInsertTags($tag);
    }
}

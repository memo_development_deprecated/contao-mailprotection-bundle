<?php

/**
 * Contao Open Source CMS
 *
 * @package   MailProtectionCaptcha
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailProtectionBundle\Module;

use Contao\CoreBundle\Exception\RedirectResponseException;
use Haste\Http\Response\RedirectResponse;
use Haste\Http\Response\Response;
use Memo\MailProtectionBundle\Model\MailProtectionModel;
use Memo\MailProtectionBundle\Services\MailProtectionService;

class MailProtectionCaptcha extends \Module
{


    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_mailprotection_captcha';


    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \Contao\BackendTemplate('be_wildcard');
            $objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend das Memo-MailProtection Captcha Formular.';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;

            return $objTemplate->parse();
        }
        return parent::generate();
    }

    protected function compile()
    {
        if (TL_MODE === 'FE') {
            $prmCaptchaPost = \Input::post('memoCaptcha');
            $prmEMailHash   = \Input::get('hash');

            //Send Captcha Form, validate Input
            $oContainer     = \System::getContainer();
            if(!is_null($prmCaptchaPost))
            {
                $sCaptchaCode   =  $oContainer->get('session')->get('memoCaptcha');

                //Validate Input
                if($sCaptchaCode == trim($prmCaptchaPost))
                {
                    $oData = MailProtectionModel::findOneBy('hash',$prmEMailHash);

                    //Return mailto Header
                    if($oData->email) {
                        $oData->click = $oData->click+1;
                        $oData->save();
                        throw new RedirectResponseException('mailto:'.$oData->email);
                    }
                }
            }
            $this->Template->blnFallback = false;
            $oImage = \FilesModel::findByUuid($this->mpbBackground);
            $sFile = $oContainer->get('memo.mailprotection.services')->_createCaptchaImage($this->length,$this->fontSize,$oImage->path);
            $sRandomString = $oContainer->get('memo.mailprotection.services')->memoRandomString($this->length);
            if($sFile) {
                $image = fopen($sFile, 'r');
                $strB64Image = base64_encode(fread($image, filesize($sFile)));
                $this->Template->captcha = "data:image/png;base64," . $strB64Image;

            }else{
                \System::getContainer()->get('session')->set('memoCaptcha',$sRandomString);
                $this->Template->blnFallback = true;
                $this->Template->mbCaptchaText = $sRandomString;
                $this->Template->captcha = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJnMSIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIHgxPSI1MCUiIHkxPSIwJSIgeDI9IjUwJSIgeTI9IjEwMCUiPjxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNmZmZmZmYiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNlM2UzZTMiLz48L2xpbmVhckdyYWRpZW50PjwvZGVmcz48cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiBmaWxsPSJ1cmwoI2cxKSIvPjwvc3ZnPg==';
            }
            $this->Template->emlHash = $prmEMailHash;

            $GLOBALS['TL_CSS'][] = 'bundles/mailprotection/css/mailProtectionBundle.css';
        }
    }
}

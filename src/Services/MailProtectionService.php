<?php
namespace Memo\MailProtectionBundle\Services;

use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\Model;
use Memo\MailProtectionBundle\Model\MailProtectionModel;
use function Memo\MailProtectionBundle\Controller\make_seed;

class MailProtectionService
{
    private $framework;

    public function __construct(ContaoFramework $framework)
    {
        $this->framework = $framework;
    }

    /**
     * @param null $prmEMailAddress
     * @return object DB Record | true = create new one | false record exist
     */
    public function updateEMailTable($prmEMailAddress = NULL)
    {
        if(is_null($prmEMailAddress))
        {
            return NULL;
        }

        $oData = MailProtectionModel::findOneBy('email',trim($prmEMailAddress));
        if(is_null($oData))
        {
            //create new Record
            $oData = new MailProtectionModel();
            $oData->email   = trim($prmEMailAddress);
            $oData->hash    = sha1(md5(microtime(1).$prmEMailAddress));
            $oData->tstamp  = time();
            $oData->save();
        }
        return $oData;
    }

    public function _createCaptchaImage($prmCharLength = 4,$prmFontSize=60,$strBackgroundImage = null)
    {

        \System::getContainer()->get('contao.framework')->initialize();
        putenv('GDFONTPATH=' . realpath('.'));

        $sRootDir   = \System::getContainer()->getParameter('kernel.project_dir')."/";
        $sCacheDir  = \System::getContainer()->getParameter('kernel.cache_dir');
        $sFileName  = \System::getContainer()->get('session')->getId().".png";
        $sRandomString = self::memoRandomString($prmCharLength);

        //set Session
        \System::getContainer()->get('session')->set('memoCaptcha',$sRandomString);

        if(!is_null($strBackgroundImage) AND is_file($sRootDir.$strBackgroundImage)) {
            $sBgImage = $sRootDir.$strBackgroundImage;
        }else{
            $sBgImage = dirname(__FILE__) . '../../Resources/public/images/default-captcha-background.png';
        }

        $tmp        = getimagesize($sBgImage);
        $iWidth     = isset($tmp[0])?$tmp[0] : 360;
        $iHeight    = isset($tmp[1])?$tmp[1] : 120;

        $img 	= ImageCreateFromPNG($sBgImage); //Backgroundimage
        $color 	= ImageColorAllocate($img, 255,255,255); //Farbe
        $font   = '/bundles/mailprotection/fonts/xfiles.ttf';
        $iAngle	= 0;//rand(0,5);

        $bbox = imagettfbbox($prmFontSize,$iAngle,$font,$sRandomString);

        //Auto Resize Font
        if($bbox[2]-$bbox[0] > $iWidth)
        {
            $prmFontSize = $prmFontSize-10;
        }

        //center Content
        $t_x = ($iWidth-($bbox[2]-$bbox[0]))/2;
        $t_y = (($iHeight+($bbox[1]-$bbox[7])-12)/2);

        imagettftext($img, $prmFontSize, $iAngle, $t_x, $t_y, $color, $font, $sRandomString);
        $img = $this->apply_wave($img,$iWidth,$iHeight);
        $tmpName = tempnam(sys_get_temp_dir(), 'img') . '.png';
        imagepng($img,$tmpName);
        imagedestroy($img);
        return $tmpName;
    }

    /**
     * Random String Generator
     * return str
     */
    public static function memoRandomString($len) : string {
        srand(self::make_memo_seed());
        //Der String $possible enthaelt alle Zeichen, die verwendet werden sollen
        $possible="ABCDEFGHJKLMNPRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789";
        $str="";
        while(strlen($str)<$len) {
            $str.=substr($possible,(rand()%(strlen($possible))),1);
        }
        return($str);
    }

    private static function make_memo_seed()
    {
        list($usec, $sec) = explode(' ', microtime());
        return (float)$sec + ((float)$usec * 100000);
    }

    /**
     * Create Wave Effect on Captcha Image
     * @param $image
     * @param $width
     * @param $height
     * @return mixed
     */
    private function apply_wave($image, $width, $height)
    {
        $x_period = 10;
        $y_period = 10;
        $y_amplitude = 3;
        $x_amplitude = 3;

        $xp = $x_period*rand(1,3);
        $k = rand(0,100);
        for ($a = 0; $a<$width; $a++)
            imagecopy($image, $image, $a-1, sin($k+$a/$xp)*$x_amplitude,
                $a, 0, 1, $height);

        $yp = $y_period*rand(1,2);
        $k = rand(0,100);
        for ($a = 0; $a<$height; $a++)
            imagecopy($image, $image, sin($k+$a/$yp)*$y_amplitude,
                $a-1, 0, $a, $width, 1);

        return $image;
    }

    public function replaceInsertTags($strTag)
    {

        $arrTags = ['memoEmail', 'memoEmail_url', 'memoEmail_url_open'];
        $chunks = explode('::', $strTag);

        //return false, inserttag not match
        if (!in_array($chunks[0],$arrTags)) {
            return false;
        }

        global $objPage;
        $sType  = '';   //Security Level (simple=contao std | extend=memologic)
        $currentRootPage = $objPage->loadDetails()->rootId;
        $objRootPage     = \PageModel::findByPk($currentRootPage);
        $sType  = $objRootPage->mpbOptions;

        $return = null;
        //Captcha page Redirect
        $prmCaptchaPageId = $objRootPage->mbpJumpTo;
        if(empty($prmCaptchaPageId))
        {
            $captchaUrl = '#';
        }else {
            $pageModel  = \PageModel::findByPK($prmCaptchaPageId);
            $captchaUrl = \Controller::generateFrontendUrl($pageModel->row());
        }

        if(!empty($chunks[1]) && $sType == 'extend') {
            $oData = self::updateEMailTable($chunks[1]);
            $maskedChunk = str_replace("@",
                    '<i style="display:none;">'.self::memoRandomString(rand(3,15)).'</i>@<i style="display:none;">'.self::memoRandomString(rand(3,15)).'</i>', trim($chunks[1]));

            switch ($chunks[0]) {
                case 'memoEmail_url':
                    $return = trim($maskedChunk);
                    break;
                case 'memoEmail_url_open':
                    $return = sprintf('<a href="%s?hash=%s" onclick="document.location.href=\'/mpb/%s\';return false;" >', $captchaUrl ,$oData->hash,$oData->hash);
                    break;
                default:
                    $return = sprintf('<a href="%s?hash=%s" onclick="document.location.href=\'/mpb/%s\';return false;" >%s</a>', $captchaUrl ,$oData->hash,$oData->hash, $maskedChunk);
            }

            return $return;
        }elseif(!empty($chunks[1]) && $sType == 'simple')
            //Return Contao Logic (sType == simple)
            switch ($chunks[0]) {
                case 'memoEmail_url': return \Contao\StringUtil::encodeEmail($chunks[1]);break;
                default: return sprintf('<a href="mailto:%s" >%s</a>', \Contao\StringUtil::encodeEmail($chunks[1]),\Contao\StringUtil::encodeEmail($chunks[1]));
        }
        return false;
    }
}

<?php
namespace Memo\MailProtectionBundle\Model;

use Contao\Model;

/**
 * Class MailProtectionModel
 *
 * Reads and writes tl_memo_mailprotection Data.
 */

class MailProtectionModel extends Model
{

    protected static $strTable = 'tl_memo_mailprotection';



}
